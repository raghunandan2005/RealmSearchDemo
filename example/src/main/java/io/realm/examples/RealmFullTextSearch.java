package io.realm.examples;

/**
 * Created by raghu on 6/2/18.
 */

import android.text.TextUtils;

import io.realm.Case;
import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;

public class RealmFullTextSearch {

    public static <T extends RealmObject> RealmResults<T> search(Realm realm, Class<T> modelClass, String query, String fieldName, boolean partialSearch){
        RealmResults<T> realmResults = realm.where(modelClass).findAll();
        if (TextUtils.isEmpty(query)) {
            return realmResults;
        }else {
      /*  String[] keywords = query.split(" ");
        for (String keyword : keywords) {
            String spacedKeyword = " " + keyword;

        }*/
            realmResults = realmResults.where().beginsWith(fieldName, query, Case.INSENSITIVE).or().contains(fieldName, query, Case.SENSITIVE).findAll();
            return realmResults;
        }
    }
}