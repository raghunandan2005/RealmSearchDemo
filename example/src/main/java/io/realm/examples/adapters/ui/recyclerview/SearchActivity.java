package io.realm.examples.adapters.ui.recyclerview;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.examples.RealmFullTextSearch;
import io.realm.examples.adapters.R;
import io.realm.examples.adapters.model.Item;
import io.realm.examples.adapters.ui.DividerItemDecoration;

/**
 * Created by raghu on 6/2/18.
 */

public class SearchActivity extends AppCompatActivity {

    private Realm realm;
    private RecyclerView recyclerView;

    private MyRecyclerViewAdapter adapter;
    private SearchView searchView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_layout);

        realm = Realm.getDefaultInstance();
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        setUpRecyclerView();

        searchView = (SearchView) findViewById(R.id.search_view);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                RealmResults<Item> results = RealmFullTextSearch.search(realm, Item.class, query, "text", false);
                Log.i("Results :",""+results.size());

                adapter.updateData(RealmFullTextSearch.search(realm, Item.class, query, "text", false));
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                //Do some magic
                return false;
            }
        });


    }

    /*
     * It is good practice to null the reference from the view to the adapter when it is no longer needed.
     * Because the <code>RealmRecyclerViewAdapter</code> registers itself as a <code>RealmResult.ChangeListener</code>
     * the view may still be reachable if anybody is still holding a reference to the <code>RealmResult>.
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        recyclerView.setAdapter(null);
        realm.close();
    }


    private void setUpRecyclerView() {

        adapter = new MyRecyclerViewAdapter( RealmFullTextSearch.search(realm, Item.class, "-1", "text", false));
        recyclerView.setLayoutManager(new LinearLayoutManager(SearchActivity.this));
        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new DividerItemDecoration(SearchActivity.this, DividerItemDecoration.VERTICAL_LIST));


    }

}
