/*
 * Copyright 2016 Realm Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.realm.examples.adapters.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Item extends RealmObject {
    public static final String FIELD_ID = "id";

    private static AtomicInteger INTEGER_COUNTER = new AtomicInteger(0);

    @PrimaryKey
    private int id;

    private String text;

    public void setText(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public int getId() {
        return id;
    }

    public String getCountString() {
        return Integer.toString(id);
    }

    //  create() & delete() needs to be called inside a transaction.
    static void create(Realm realm) {
        create(realm, false);
    }

    static void create(Realm realm, boolean randomlyInsert) {
        addStrings();
        Parent parent = realm.where(Parent.class).findFirst();
        RealmList<Item> items = parent.getItemList();
        Item counter = realm.createObject(Item.class, increment());
        counter.setText(anyItem());
        if (randomlyInsert && items.size() > 0) {
            Random rand = new Random();
            items.listIterator(rand.nextInt(items.size())).add(counter);
        } else {
            items.add(counter);

        }
    }

    static void delete(Realm realm, long id) {
        Item item = realm.where(Item.class).equalTo(FIELD_ID, id).findFirst();
        // Otherwise it has been deleted already.
        if (item != null) {
            item.deleteFromRealm();
        }
    }

    static void modify(Realm realm, long id) {

        Item item = realm.where(Item.class).equalTo(FIELD_ID, id).findFirst();
        // Otherwise it has been deleted already.
        if (item != null) {
            item.text = "Modified Text";
        }
    }

    private static int increment() {
        return INTEGER_COUNTER.getAndIncrement();
    }

    private static List<String> listStrings;
    public static List<String> addStrings(){
        listStrings = new ArrayList<String>();
        listStrings.add("Lorem Ipsum is simply dummy text of the printing and typesetting industry");
        listStrings.add("Lorem Ipsum has been the industry's standard dummy text ever since the 1500s");
        listStrings.add("when an unknown printer took a galley of type and scrambled it to make a type specimen book");
        listStrings.add("It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.");
        listStrings.add("It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.");
        listStrings.add("Why do we use it?");
        listStrings.add("It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.");
        listStrings.add("Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.");
        listStrings.add("The point of usingMany desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy.");
        listStrings.add("Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).");
        listStrings.add("Where does it come from?");
        listStrings.add("Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old");
        listStrings.add("Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.");
        listStrings.add("Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of de Finibus Bonorum et Malorum (The Extremes of Good and Evil) by Cicero, written in 45 BC.");
        listStrings.add("This book is a treatise on the theory of ethics, very popular during the Renaissance.");
        listStrings.add("The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.");
        listStrings.add("The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested.");
        listStrings.add("Sections 1.10.32 and 1.10.33 from \"de Finibus Bonorum et Malorum\" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.");
        listStrings.add("Where can I get some?");
        listStrings.add("There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.");
        listStrings.add("If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text.");
        listStrings.add(" All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet.");
        listStrings.add("It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable.");
        listStrings.add("The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.");

        return listStrings;
    }

    private static Random randomGenerator = new Random();
    public static  String anyItem()
    {
        int index = randomGenerator.nextInt(listStrings.size());
        String item = listStrings.get(index);
        return item;
    }
}
